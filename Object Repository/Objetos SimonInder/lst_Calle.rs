<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Click para que se muestren las opciones del tipo de dirección</description>
   <name>lst_Calle</name>
   <tag></tag>
   <elementGuidId>16ecb2be-53dd-4674-bca7-982e4edeb86a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 's2id_formulario_registro_direccion_format_tipo_via']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>s2id_formulario_registro_direccion_format_tipo_via</value>
   </webElementProperties>
</WebElementEntity>
