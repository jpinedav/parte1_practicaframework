<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Click en el Radio Button para elegir Barrio</description>
   <name>rbt_Barrio</name>
   <tag></tag>
   <elementGuidId>19529407-a4bc-44cc-be77-66606d30ab42</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>/html/body/div[1]/div[2]/section/section/form/div/div[3]/div[2]/div/div[2]/div/div[2]/ins</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html/body/div[1]/div[2]/section/section/form/div/div[3]/div[2]/div/div[2]/div/div[2]/ins</value>
   </webElementProperties>
</WebElementEntity>
