<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Campo en el que se ingresa el número de documento</description>
   <name>cmp_Identificacion</name>
   <tag></tag>
   <elementGuidId>8db0afd4-8287-4f85-b755-f257f09c4293</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'numeroidentificacion']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>numeroidentificacion</value>
   </webElementProperties>
</WebElementEntity>
