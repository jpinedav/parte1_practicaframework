<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Campo en el que se ingresa la clave</description>
   <name>cmp_Clave</name>
   <tag></tag>
   <elementGuidId>a6389af4-97e3-4c0d-b6ae-c467d4f89664</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'clave_uno']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>clave_uno</value>
   </webElementProperties>
</WebElementEntity>
