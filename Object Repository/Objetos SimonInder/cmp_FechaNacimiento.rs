<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Campo en el que se ingresa la fehca de nacimiento</description>
   <name>cmp_FechaNacimiento</name>
   <tag></tag>
   <elementGuidId>36abd8a2-7c60-47c8-8632-5d8e3125e386</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'fechanacimiento']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>fechanacimiento</value>
   </webElementProperties>
</WebElementEntity>
