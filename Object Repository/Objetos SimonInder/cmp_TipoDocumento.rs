<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Campo en el que se ingresa el tipo de documento de identidad</description>
   <name>cmp_TipoDocumento</name>
   <tag></tag>
   <elementGuidId>0a6cb15a-9f83-43e4-925d-8589bf51c441</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 's2id_autogen16_search']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>s2id_autogen16_search</value>
   </webElementProperties>
</WebElementEntity>
