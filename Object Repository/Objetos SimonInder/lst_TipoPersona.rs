<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Para desplegar la lista de tipo de persona</description>
   <name>lst_TipoPersona</name>
   <tag></tag>
   <elementGuidId>cf105429-ace8-42da-96af-b51eb5743f03</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 's2id_tipopersona']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>s2id_tipopersona</value>
   </webElementProperties>
</WebElementEntity>
