<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Campo en el que se ingresa el número de Estrato</description>
   <name>cmp_Estrato</name>
   <tag></tag>
   <elementGuidId>a75e46eb-d2c0-4924-96c2-ec03a4fbd9d5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 's2id_autogen28_search']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>s2id_autogen28_search</value>
   </webElementProperties>
</WebElementEntity>
