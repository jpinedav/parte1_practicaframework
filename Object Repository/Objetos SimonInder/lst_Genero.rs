<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Click en la lista de Sexo para seleccionar si es hombre o mujer</description>
   <name>lst_Genero</name>
   <tag></tag>
   <elementGuidId>d075fca6-9a00-43f0-9e81-5a20791f401b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'select2-chosen-17']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>select2-chosen-17</value>
   </webElementProperties>
</WebElementEntity>
