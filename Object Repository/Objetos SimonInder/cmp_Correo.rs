<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Ingresar el correo electrónico</description>
   <name>cmp_Correo</name>
   <tag></tag>
   <elementGuidId>5bf9b262-7f7b-4ecc-afcf-93326e574564</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'correoelectronico']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>correoelectronico</value>
   </webElementProperties>
</WebElementEntity>
