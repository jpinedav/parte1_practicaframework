<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Click en el botón de la lista que corresponde a Medellin</description>
   <name>btn_Medellin</name>
   <tag></tag>
   <elementGuidId>8462b43d-44f3-4ef1-8e15-325316cbce6c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>/html/body/div[7]/ul/li/div/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html/body/div[7]/ul/li/div/span</value>
   </webElementProperties>
</WebElementEntity>
