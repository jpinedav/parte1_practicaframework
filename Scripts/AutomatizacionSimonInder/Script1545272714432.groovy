import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://simon.inder.gov.co/registro')

WebUI.click(findTestObject('Objetos SimonInder/lst_TipoPersona'))

WebUI.click(findTestObject('Objetos SimonInder/btn_PersonaNatural'))

WebUI.click(findTestObject('Objetos SimonInder/lst_TipoDeDocumento'))

WebUI.click(findTestObject('Objetos SimonInder/btn_Documento'))

WebUI.sendKeys(findTestObject('Objetos SimonInder/cmp_Identificacion'), '1017122877')

WebUI.sendKeys(findTestObject('Objetos SimonInder/cmp_Nombres'), 'Jhon')

WebUI.sendKeys(findTestObject('Objetos SimonInder/cmp_Apellidos'), 'Pineda')

WebUI.click(findTestObject('Objetos SimonInder/lst_Genero'))

WebUI.click(findTestObject('Objetos SimonInder/btn_Sexo'))

WebUI.sendKeys(findTestObject('Objetos SimonInder/cmp_FechaNacimiento'), '23121985')

WebUI.sendKeys(findTestObject('Objetos SimonInder/cmp_Clave'), 'FIcansee123')

WebUI.sendKeys(findTestObject('Objetos SimonInder/cmp_ConfirmaClave'), 'FIcansee123')

WebUI.click(findTestObject('Objetos SimonInder/lst_Municipio'))

WebUI.sendKeys(findTestObject('Objetos SimonInder/cmp_Municipio'), 'Medellin')

WebUI.click(findTestObject('Objetos SimonInder/btn_Medellin'))

WebUI.click(findTestObject('Objetos SimonInder/lst_Estrato'))

WebUI.sendKeys(findTestObject('Objetos SimonInder/cmp_Estrato'), '3')

WebUI.click(findTestObject('Objetos SimonInder/btn_Estrato'))

WebUI.click(findTestObject('Objetos SimonInder/rbt_Barrio'))

WebUI.click(findTestObject('Objetos SimonInder/lst_Calle'))

WebUI.sendKeys(findTestObject('Objetos SimonInder/cmp_Calle'), 'Calle')

WebUI.click(findTestObject('Objetos SimonInder/btn_Calle'))

WebUI.sendKeys(findTestObject('Objetos SimonInder/cmp_NumeroCalle'), '40')

WebUI.sendKeys(findTestObject('Objetos SimonInder/cmp_NumeroP1'), '10')

WebUI.sendKeys(findTestObject('Objetos SimonInder/cmp_NumeroP2'), '20')

WebUI.sendKeys(findTestObject('Objetos SimonInder/cmp_Correo'), 'jhonpineda20@hotmail.com')

WebUI.sendKeys(findTestObject('Objetos SimonInder/cmp_Telefono'), '3004332323')

WebUI.click(findTestObject('Objetos SimonInder/cbx_Politicas'))

WebUI.click(findTestObject('Objetos SimonInder/cbx_Terminos'))

WebUI.delay(5)

WebUI.click(findTestObject('Objetos SimonInder/lst_Barrios'))

WebUI.sendKeys(findTestObject('Objetos SimonInder/cmp_Barrio'), 'Buenos Aires')

WebUI.click(findTestObject('Objetos SimonInder/btn_Barrio'))

WebUI.click(findTestObject('Objetos SimonInder/btn_Guardar'))

WebUI.delay(5)

WebUI.closeBrowser()

